import dataclasses
import subprocess
import typing
from subprocess import CompletedProcess
from unittest.mock import MagicMock

import pytest


def default_return_values():
    return [CompletedProcess(RunCommandFaker.FAKE_COMMAND, returncode=0)]


@dataclasses.dataclass
class RunCommandFaker:
    FAKE_COMMAND = "FAKE_COMMAND"

    mock: typing.Optional[MagicMock] = None

    _return_values: typing.List[CompletedProcess] = dataclasses.field(
        default_factory=default_return_values
    )

    def set_mock(self, mock: MagicMock):
        self.mock = mock

    def set_result(
        self,
        exit_code: int = 0,
        stdout: typing.Union[str, bytes] = b"",
        stderr: typing.Union[str, bytes] = b"",
    ) -> None:
        if not self.mock:
            raise AttributeError("mock not set")

        if isinstance(stdout, str):
            stdout = stdout.encode()

        if isinstance(stderr, str):
            stderr = stderr.encode()

        self._return_values = [
            CompletedProcess(
                args=RunCommandFaker.FAKE_COMMAND,
                returncode=exit_code,
                stdout=stdout,
                stderr=stderr,
            )
        ]

    def raise_errors_if_applicable(self, *args, **kwargs):
        check = kwargs.get("check", False)
        result = self._return_values.pop(0)
        if check and result.returncode:
            raise subprocess.CalledProcessError(
                cmd=RunCommandFaker.FAKE_COMMAND, returncode=result.returncode
            )
        return result


_run_command_faker = RunCommandFaker()


@pytest.fixture
def mocked_subprocess_run(mocker):
    mocked = mocker.patch("subprocess.run")
    _run_command_faker.set_mock(mocked)
    mocked.side_effect = _run_command_faker.raise_errors_if_applicable
    return mocked


@pytest.fixture
def run_command_faker(mocked_subprocess_run):
    return _run_command_faker
