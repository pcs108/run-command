import subprocess
import typing

DEFAULT_KWARGS = {"shell": True, "capture_output": False}


def run_command(command: str, **kwargs) -> typing.Optional[str]:
    kwargs = {**DEFAULT_KWARGS, **kwargs}
    result = subprocess.run(command, **kwargs)
    if kwargs.get("capture_output"):
        output = result.stdout.decode()
        if not output:
            output = result.stderr.decode()
        return output
