import shutil

import pytest

from run_command.plugin import *  # noqa
from tests import TEST_DATA


@pytest.fixture(autouse=True)
def clean_up_test_files():
    def _clean_up():
        if TEST_DATA.exists():
            shutil.rmtree(TEST_DATA)

    _clean_up()
    TEST_DATA.mkdir()
    yield
    _clean_up()
