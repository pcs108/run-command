from run_command import run_command

from . import TEST_DATA

TEST_FILE = TEST_DATA / "hello.txt"


def test_run_command():
    assert run_command(f"touch {str(TEST_FILE)!r}") is None
    assert TEST_FILE.exists()


def test_run_command_returns_output_when_specified():
    TEST_FILE.write_text("")
    assert run_command(f"ls {str(TEST_DATA)!r}", capture_output=True) == "hello.txt\n"


def test_run_command_returns_stderr_if_stdout_is_empty():
    TEST_FILE.write_text("")
    assert run_command(f"ffprobe {str(TEST_FILE)}", capture_output=True)
