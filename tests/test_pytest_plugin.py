from subprocess import CalledProcessError

import pytest

from run_command import DEFAULT_KWARGS, run_command


@pytest.fixture(autouse=True)
def use_plugin_mocker(run_command_faker):
    return run_command_faker


def test_run_command_faker_mocks_subprocess_run(mocked_subprocess_run):
    run_command("hello")
    mocked_subprocess_run.assert_called_with("hello", **DEFAULT_KWARGS)


def test_run_command_faker_set_result_for_stdout(
    mocked_subprocess_run, run_command_faker
):
    run_command_faker.set_result(stdout="world")
    assert run_command("hello", capture_output=True) == "world"


def test_run_command_faker_raises_error_for_failed_result_when_check_is_true(
    run_command_faker,
):
    run_command_faker.set_result(exit_code=127)
    with pytest.raises(CalledProcessError):
        run_command("hello", check=True)
